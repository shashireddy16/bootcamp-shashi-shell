**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
# connecting to valhalla cluster node
ssh sreddy@edge1.valhalla.phdata.io
VPN - vpn.valhalla.phdata.io


#!/bin/bash - "to code highlight the shell script"
code editor VIM - syntax highlight advantage
echo $? - " to check exit status of command"
chmod +x myscript - " to give permission for execution"
chmod 777 <filename>
echo $PATH

Exercise 2:=========================================
COLOR = blue - "to store value in variable"
echo $COLOR - "to see the value of variable"
bash - "to open sub shell"
export COLOR = red - "to make variable available in all shells"

ps aux
ps aux | grep http |awk '{ print $2 }'
bash -x tokill

echo $$ -"process id"

Exercise 3:====================================================
Some matching operators
${VAR#pattern} - search for beginning of the pattern and delete shortest part that matches.
${VAR##pattern} - Search for pattern from beginning of variable and delete the longest part that matches.
${VAR%pattern} - pattern match end of the variable, delete the shortest part that matches.
${VAR%%pattern} - pattern matches the end of the variables value, delete the longest part that matches return's rest.

Regular expressions:
grep, awk, sed
overview: ^text, text$, ., [abc], [a-c], *, \{2\}, \{1,3\}, colou?r

Exercise_4: ==================================================
Transform the string cn=lara, dc=example, dc=com in a way
user name (lara) extracted from the string.
make sure result is written in all lower case
store the user name in variable USER 
end of the script echo the USER variable.

basic tools grep,test, cut and sort, tall and head
advanced tools sed, awk, tr

grep - search for text pattern based on regular expressions
grep - general regular expression parser.
grep -i -e date -e year

test - testing items or strings
test -z $1, test $1 = 6, test file1 -nt file2, test -x file1
single square brackets and double square brackets

using cut and sort 
cut to filter specific field or column out of line
sort to sort data 
cut and sort are often seen together
cut -f 1 -d : /etc/passwd | sort -n
command field num delimiter : PATH
du -h |sort -rn
sort -n -k2 -t " /etc/passwd"

using tail and head
head -5 /etc/passwd | tail -1

using sed 
stream line editor, it is more than text processing, its a programming language
sed -n 5p /etc/passwd
sed 's/t/T/g' <filepath>

Using awk:


using tr:
(translate) helps in transforming strings
echo hello | tr [a-z][A-Z]
echo hello | tr [:lower:][:upper:]

LDAP format is simple plain text
Exercise_5: ==========================================
conditional statements in script:

if expression
then
    command 1
else
    command 3
fi

if expression
then
    command 1
elif
then
    command 2
fi

Using && and ||
[ -z $1 ] && echo no argument provided && exit 2
[ -f $1 ] && echo $1 is a file && exit 0
[ -d $1 ] && echo $1 is a directory && exit 0

> <filename> - "to clean the content in file"






